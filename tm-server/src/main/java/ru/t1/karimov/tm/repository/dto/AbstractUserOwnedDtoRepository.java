package ru.t1.karimov.tm.repository.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.karimov.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.karimov.tm.dto.model.AbstractUserOwnedDtoModel;

@Getter
@Repository
@Scope("prototype")
@AllArgsConstructor
public abstract class AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedDtoModel>
        extends AbstractDtoRepository<M> implements IUserOwnedDtoRepository<M> {

    @NotNull
    protected static final String USER_ID = "userId";

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) throws Exception {
        model.setUserId(userId);
        entityManager.persist(model);
        return model;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) throws Exception {
        return findOneById(userId, id) != null;
    }

    @Override
    public void removeOne(@NotNull final String userId, @NotNull final M model) throws Exception {
        @NotNull final String delUserId = model.getUserId();
        if (!delUserId.equals(userId)) return;
        entityManager.remove(model);
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return;
        removeOne(userId, model);
    }

    @Override
    public void removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) throws Exception {
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return;
        removeOne(userId, model);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) throws Exception {
        model.setUserId(userId);
        update(model);
    }

}
