package ru.t1.karimov.tm.repository.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.karimov.tm.api.repository.model.ITaskRepository;
import ru.t1.karimov.tm.model.Task;

import javax.persistence.TypedQuery;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    private static final String PROJECT_ID = "projectId";

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        @NotNull final Task project = new Task();
        project.setName(name);
        project.setDescription(description);
        return add(userId, project);
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name) throws Exception {
        @NotNull final Task project = new Task();
        project.setName(name);
        return add(userId, project);
    }

    @Override
    public Long getSize() throws Exception {
        @NotNull final String jpql = "SELECT count(t) FROM Task t";
        @NotNull final TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
        return query.getSingleResult();
    }

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        @NotNull final String jpql = "SELECT t FROM Task t";
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(jpql, Task.class);
        return query.getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final Comparator<Task> comparator) throws Exception {
        @NotNull final String sort = getSortType(comparator);
        @NotNull final String jpql = "SELECT t FROM Task t ORDER BY t." + sort;
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(jpql, Task.class);
        return query.getResultList();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String id) throws Exception {
        return entityManager.find(Task.class, id);
    }

    @Nullable
    @Override
    public Task findOneByIndex(@NotNull final Integer index) throws Exception {
        @NotNull final String jpql = "SELECT t FROM Task t";
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(jpql, Task.class)
                .setFirstResult(index);
        return query.getSingleResult();
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "SELECT t FROM Task t WHERE t.user.id = :userId";
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(jpql, Task.class)
                .setParameter(USER_ID, userId);
        return query.getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId, @NotNull final Comparator<Task> comparator
    ) throws Exception {
        @NotNull final String sort = getSortType(comparator);
        @NotNull final String jpql = "SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY t." + sort;
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(jpql, Task.class)
                .setParameter(USER_ID, userId);
        return query.getResultList();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String jpql = "SELECT t FROM Task t WHERE t.id = :id AND t.user.id = :userId";
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(jpql, Task.class)
                .setParameter(USER_ID, userId)
                .setParameter(ID, id)
                .setMaxResults(1);
        return query.getResultList()
                .stream().findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public Task findOneByIndex(@NotNull final String userId, @NotNull final Integer index) throws Exception {
        @NotNull final String jpql = "SELECT t FROM Task t WHERE t.user.id = :userId";
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(jpql, Task.class)
                .setParameter(USER_ID, userId)
                .setFirstResult(index);
        return query.getResultList()
                .stream().findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull String jpql = "SELECT t FROM Task t WHERE t.user.id = :userId AND t.project.id = :projectId";
        @NotNull final TypedQuery<Task> query = entityManager.createQuery(jpql, Task.class)
                .setParameter(USER_ID, userId)
                .setParameter(PROJECT_ID, projectId);
        return query.getResultList();
    }

    @Override
    public Long getSize(@NotNull final String userId) throws Exception {
        @NotNull String jpql = "SELECT COUNT(t) FROM Task t WHERE t.user.id = :userId";
        @NotNull final TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class)
                .setParameter(USER_ID, userId);
        return query.getSingleResult();
    }

}
