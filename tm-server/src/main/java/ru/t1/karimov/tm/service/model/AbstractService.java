package ru.t1.karimov.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.karimov.tm.api.repository.model.IRepository;
import ru.t1.karimov.tm.api.service.model.IService;
import ru.t1.karimov.tm.exception.entity.EntityNotFoundException;
import ru.t1.karimov.tm.exception.field.IdEmptyException;
import ru.t1.karimov.tm.exception.field.IndexIncorrectException;
import ru.t1.karimov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractService<M extends AbstractModel> implements IService<M> {

    @NotNull
    protected static final String ERROR_INDEX_OUT_OF_BOUNDS = "Error! Index оut of bounds...";

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    protected EntityManager getEntityManager() {
        return context.getBean(EntityManager.class);
    }

    protected abstract IRepository<M> getRepository();

    @NotNull
    @Override
    public M add(@Nullable final M model) throws Exception {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository();
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository();
            entityManager.getTransaction().begin();
            @NotNull final List<M> mList = new ArrayList<>(repository.add(models));
            entityManager.getTransaction().commit();
            return mList;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository();
            return repository.existsById(id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository();
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws Exception {
        if (comparator == null) return findAll();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository();
            return repository.findAll(comparator);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository();
            return repository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) throws Exception {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize())
            throw new IndexIncorrectException(ERROR_INDEX_OUT_OF_BOUNDS);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository();
            return repository.findOneByIndex(index);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Long getSize() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository();
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository();
            entityManager.getTransaction().begin();
            repository.removeAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOne(@Nullable final M model) throws Exception {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository();
            entityManager.getTransaction().begin();
            repository.removeOne(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository();
            entityManager.getTransaction().begin();
            repository.removeOneById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneByIndex(@Nullable final Integer index) throws Exception {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize())
            throw new IndexIncorrectException(ERROR_INDEX_OUT_OF_BOUNDS);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository();
            entityManager.getTransaction().begin();
            repository.removeOneByIndex(index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository();
            entityManager.getTransaction().begin();
            @NotNull final List<M> mList = new ArrayList<>(repository.set(models));
            entityManager.getTransaction().commit();
            return mList;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@Nullable final M model) throws Exception {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IRepository<M> repository = getRepository();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
