package ru.t1.karimov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.AbstractUserOwnedDtoModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedDtoRepository<M extends AbstractUserOwnedDtoModel> extends IDtoRepository<M> {

    @NotNull
    M add(@NotNull String userId, @NotNull M model) throws Exception;

    @NotNull
    List<M> findAll(@NotNull String userId) throws Exception;

    @NotNull
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator) throws Exception;

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id) throws Exception;

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index) throws Exception;

    boolean existsById(@NotNull String userId, @NotNull String id) throws Exception;

    Long getSize(@NotNull String userId) throws Exception;

    void removeOneById(@NotNull String userId, @NotNull String id) throws Exception;

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index) throws Exception;

    void removeOne(@NotNull String userId, @NotNull M model) throws Exception;

    void removeAll(@NotNull String userId) throws Exception;

    void update(@NotNull String userId, M model) throws Exception;

}
