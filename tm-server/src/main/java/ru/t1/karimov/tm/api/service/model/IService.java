package ru.t1.karimov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @NotNull
    M add(M model) throws Exception;

    @NotNull
    Collection<M> add(@NotNull Collection<M> models) throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @NotNull
    List<M> findAll() throws Exception;

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator) throws Exception;

    @Nullable
    M findOneById(@Nullable String id) throws Exception;

    @Nullable
    M findOneByIndex(@Nullable Integer index) throws Exception;

    Long getSize() throws Exception;

    void removeAll() throws Exception;

    void removeOne(M model) throws Exception;

    void removeOneById(@Nullable String id) throws Exception;

    void removeOneByIndex(@Nullable Integer index) throws Exception;

    @NotNull
    Collection<M> set(@NotNull Collection<M> models) throws Exception;

    void update(M model) throws Exception;

}
