package ru.t1.karimov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.request.project.ProjectCompleteByIndexRequest;
import ru.t1.karimov.tm.util.TerminalUtil;

@Component
public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Complete project by index.";

    @NotNull
    public static final String NAME = "project-complete-by-index";

    @Override
    public void execute() throws Exception {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() -1;
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(getToken());
        request.setIndex(index);
        projectEndpoint.completeProjectByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
