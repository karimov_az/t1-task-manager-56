package ru.t1.karimov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.request.user.UserLoginRequest;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.util.TerminalUtil;

@Component
public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "User login.";

    @NotNull
    public static final String NAME = "login";

    @Override
    public void execute() throws Exception {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        @Nullable final String token = authEndpoint.login(request).getToken();
        setToken(token);
        System.out.println(token);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
