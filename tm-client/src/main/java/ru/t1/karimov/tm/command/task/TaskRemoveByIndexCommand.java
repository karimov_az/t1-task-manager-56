package ru.t1.karimov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.request.task.TaskRemoveByIndexRequest;
import ru.t1.karimov.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Remove task by index.";

    @NotNull
    public static final String NAME = "task-remove-by-index";

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() -1;
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(getToken());
        request.setIndex(index);
        taskEndpoint.removeTaskByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
