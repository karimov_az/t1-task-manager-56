package ru.t1.karimov.tm.command.data;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.karimov.tm.command.AbstractCommand;

@Component
public abstract class AbstractDataCommand extends AbstractCommand {

    @Autowired
    protected IDomainEndpoint domainEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
