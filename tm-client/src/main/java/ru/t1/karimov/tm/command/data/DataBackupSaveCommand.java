package ru.t1.karimov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.request.domain.DataBackupSaveRequest;
import ru.t1.karimov.tm.enumerated.Role;

@Component
public final class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Save backup to file.";

    @NotNull
    public static final String NAME = "backup-save";

    @Override
    @SneakyThrows
    public void execute() {
        domainEndpoint.saveDataBackup(new DataBackupSaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
