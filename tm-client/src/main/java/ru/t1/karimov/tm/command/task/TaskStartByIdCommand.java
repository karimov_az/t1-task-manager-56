package ru.t1.karimov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.request.task.TaskStartByIdRequest;
import ru.t1.karimov.tm.util.TerminalUtil;

@Component
public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Start task by id.";

    @NotNull
    public static final String NAME = "task-start-by-id";

    @Override
    public void execute() throws Exception {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(getToken());
        request.setId(id);
        taskEndpoint.startTaskById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
