package ru.t1.karimov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.model.TaskDto;
import ru.t1.karimov.tm.dto.request.task.TaskGetByIndexRequest;
import ru.t1.karimov.tm.dto.response.task.TaskGetByIndexResponse;
import ru.t1.karimov.tm.util.TerminalUtil;

@Component
public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Show task by index.";

    @NotNull
    public static final String NAME = "task-show-by-index";

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() -1;
        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(getToken());
        request.setIndex(index);
        @NotNull final TaskGetByIndexResponse response = taskEndpoint.getTaskByIndex(request);
        @Nullable final TaskDto task = response.getTask();
        showTask(task);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
