package ru.t1.karimov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.model.UserDto;
import ru.t1.karimov.tm.dto.request.user.UserUpdateProfileRequest;
import ru.t1.karimov.tm.dto.response.user.UserUpdateProfileResponse;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.util.TerminalUtil;

@Component
public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Update profile of current user.";

    @NotNull
    public static final String NAME = "user-update-profile";

    @Override
    public void execute() throws Exception {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("ENTER FIRST NAME:");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        @Nullable final String middleName = TerminalUtil.nextLine();
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(getToken());
        request.setFirstName(firstName);
        request.setLastName(lastName);
        request.setMiddleName(middleName);
        @NotNull final UserUpdateProfileResponse response = userEndpoint.updateUserProfile(request);
        @Nullable final UserDto user = response.getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
